package ru.t1.malyugin.tm.migration;

import liquibase.Liquibase;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.malyugin.tm.configuration.ServerConfiguration;

public abstract class AbstractSchemeTest {

    protected static Liquibase LIQUIBASE;

    @BeforeClass
    public static void before() {
        ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        LIQUIBASE = context.getBean(Liquibase.class);
    }

}