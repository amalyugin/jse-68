package ru.t1.malyugin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.malyugin.tm.dto.model.AbstractWBSDTOModel;

import java.util.List;

@NoRepositoryBean
public interface AbstractWBSDTORepository<M extends AbstractWBSDTOModel> extends AbstractUserOwnedDTORepository<M> {

    @NotNull
    List<M> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

}