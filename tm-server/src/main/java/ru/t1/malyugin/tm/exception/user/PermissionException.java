package ru.t1.malyugin.tm.exception.user;

public final class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! No permissions...");
    }

}