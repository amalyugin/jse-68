package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.dto.soap.*;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

@Endpoint
public class TaskSoapEndpointImpl {

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public static final String NAMESPACE = "http://tm.malyugin.t1.ru/dto/soap";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        final TaskFindByIdResponse response = new TaskFindByIdResponse();
        final Task task = taskService.findById(request.getId());
        response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        final TaskFindAllResponse response = new TaskFindAllResponse();
        final List<Task> tasks = (List<Task>) taskService.findAll();
        response.setTasks(tasks);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        taskService.clear();
        return new TaskClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.deleteById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    private TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        taskService.add(request.getTask());
        return new TaskSaveResponse();
    }

}