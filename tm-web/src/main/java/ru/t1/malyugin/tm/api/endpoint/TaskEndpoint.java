package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.model.Task;

import javax.jws.WebParam;
import java.util.Collection;

@RequestMapping("/api/task")
public interface TaskEndpoint {

    @GetMapping("/get/{id}")
    Task findById(
            @PathVariable String id
    );

    @GetMapping("/get")
    Collection<Task> findAll();

    @DeleteMapping("/delete/all")
    void clear();

    @PostMapping("/post")
    void save(
            @RequestBody Task task
    );

    @DeleteMapping("/delete/{id}")
    void deleteById(
            @PathVariable String id
    );

}