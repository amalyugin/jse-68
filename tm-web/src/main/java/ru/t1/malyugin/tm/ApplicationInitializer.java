package ru.t1.malyugin.tm;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.malyugin.tm.configuration.ApplicationConfiguration;
import ru.t1.malyugin.tm.configuration.DatabaseConnectionConfiguration;
import ru.t1.malyugin.tm.configuration.ThymeleafConfiguration;
import ru.t1.malyugin.tm.configuration.WebConfig;

import javax.servlet.ServletContext;

public final class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                ApplicationConfiguration.class, DatabaseConnectionConfiguration.class
        };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                ThymeleafConfiguration.class, WebConfig.class
        };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void registerContextLoaderListener(final ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}