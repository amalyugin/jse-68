package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.ProjectEndpoint;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.model.Project;

import javax.jws.WebParam;
import java.util.Collection;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/get/{id}")
    public Project findById(
            @PathVariable("id") String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @GetMapping("/get")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @DeleteMapping("/delete/all")
    public void clear() {
        projectService.clear();
    }

    @Override
    @PostMapping("/post")
    public void save(
            @RequestBody Project project
    ) {
        projectService.add(project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @PathVariable("id") String id
    ) {
        projectService.deleteById(id);
    }

}