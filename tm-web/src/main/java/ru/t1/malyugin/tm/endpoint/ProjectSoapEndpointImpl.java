package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.dto.soap.*;
import ru.t1.malyugin.tm.model.Project;

import java.util.List;

/*
 * http://localhost:8080/ws/projectEndpoint.wsdl
 */
@Endpoint
public class ProjectSoapEndpointImpl {

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public static final String NAMESPACE = "http://tm.malyugin.t1.ru/dto/soap";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        final Project project = projectService.findById(request.getId());
        response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        final ProjectFindAllResponse response = new ProjectFindAllResponse();
        final List<Project> projects = (List<Project>) projectService.findAll();
        response.setProjects(projects);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@RequestPayload final ProjectClearRequest request) {
        projectService.clear();
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    private ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        projectService.add(request.getProject());
        return new ProjectSaveResponse();
    }

}