package ru.t1.malyugin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.api.endpoint.TaskEndpoint;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.model.Task;

import javax.jws.WebParam;
import java.util.Collection;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpointImpl implements TaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping("/get/{id}")
    public Task findById(
            @PathVariable("id") String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @GetMapping("/get")
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @DeleteMapping("/delete/all")
    public void clear() {
        taskService.clear();
    }

    @Override
    @PostMapping("/post")
    public void save(
            @RequestBody Task task
    ) {
        taskService.add(task);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @PathVariable("id") String id
    ) {
        taskService.deleteById(id);
    }

}