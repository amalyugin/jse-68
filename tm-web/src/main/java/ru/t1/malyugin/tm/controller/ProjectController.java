package ru.t1.malyugin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Project;

import java.util.Collection;

@Controller
public class ProjectController {

    private final static String REDIRECT_TO_LIST = "redirect:/projects";
    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    public String showProjectList(Model model) {
        Collection<Project> projects = projectService.findAll();
        model.addAttribute("projects", projects);
        return "list/projectList";
    }

    @GetMapping("/project/create")
    public String create() {
        projectService.create();
        return REDIRECT_TO_LIST;
    }

    @GetMapping("/project/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectService.deleteById(id);
        return REDIRECT_TO_LIST;
    }

    @PostMapping("/project/edit/{id}")
    public String edit(@ModelAttribute("project") Project project) {
        projectService.edit(project);
        return REDIRECT_TO_LIST;
    }

    @GetMapping("/project/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        final Project project = projectService.findById(id);
        model.addAttribute("project", project);

        final Status[] statuses = Status.values();
        model.addAttribute("statuses", statuses);

        return "/edit/projectEdit";
    }

}