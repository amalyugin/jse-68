package ru.t1.malyugin.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.malyugin.tm.model.Project;

import javax.jws.WebParam;
import java.util.Collection;

@RequestMapping("/api/project")
public interface ProjectEndpoint {

    @GetMapping("/get/{id}")
    Project findById(
            @PathVariable String id
    );

    @GetMapping("/get")
    Collection<Project> findAll();

    @DeleteMapping("/delete/all")
    void clear();

    @PostMapping("/post")
    void save(
            @RequestBody Project project
    );

    @DeleteMapping("/delete/{id}")
    void deleteById(
            @PathVariable String id
    );

}