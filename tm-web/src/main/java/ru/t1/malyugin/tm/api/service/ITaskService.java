package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.model.Task;

import java.util.Collection;

public interface ITaskService {

    Collection<Task> findAll();

    long count();

    void create();

    void add(Task task);

    void deleteById(String id);

    void deleteAll(Collection<Task> tasks);

    void clear();

    void edit(Task task);

    Task findById(String id);

}