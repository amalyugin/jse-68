package ru.t1.malyugin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.request.task.TaskShowListByProjectIdRequest;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskListShowByProjectIdListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-show-by-project-id";

    @NotNull
    private static final String DESCRIPTION = "Show task list by project id";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskListShowByProjectIdListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[SHOW TASK LIST BY PROJECT ID]");

        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String projectId = TerminalUtil.nextLine();

        @NotNull final TaskShowListByProjectIdRequest request = new TaskShowListByProjectIdRequest(getToken(), projectId);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.showTaskListByProject(request).getTaskList();
        renderTaskList(tasks);
    }

}